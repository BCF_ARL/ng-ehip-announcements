import angular from 'angular';
import 'restangular';

export var announcementServiceModule = angular.module('announcementServiceModule', ['restangular'])
    .service('AnnouncementService', ['Restangular',
        function(Restangular) {
            var service = {
                announcements: [],
                getAll: function() {
                    return Restangular.all('announcements').getList().then(function(res) {
                        service.announcements = angular.copy(res);
                        return res;
                    });
                },
                dismiss: function(announcementID, type) {
                    return Restangular.one('announcement', announcementID).all('dismiss').post({
                        type: type
                    }).then(function(res) {
                        service.announcements = _.filter(service.announcements, function(announcement) {
                            return announcement.id !== announcementID;
                        });
                    });
                },
            };
            service.getAll();
            return service;
        }
    ]);
